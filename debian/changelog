scythe (0.994+git20141017.20d3cff-5) unstable; urgency=medium

  * Team upload.
  * Build-depend on discount instead of makdown (Closes: #1073103)
  * Set upstream metadata fields: Bug-Submit.

 -- Charles Plessy <plessy@debian.org>  Fri, 14 Jun 2024 08:54:07 +0900

scythe (0.994+git20141017.20d3cff-4) unstable; urgency=medium

  * Team upload.
  * d/control: declare compliance to standards version 4.7.0.
  * d/{control,rules}: make documentation aware of nodoc profile.
    This prevents production of the README.html and in return allows
    running builds without markdown for the nodoc build profile.
  * d/clean: new: cleanup README.html. (Closes: #1048084)

 -- Étienne Mollier <emollier@debian.org>  Sun, 12 May 2024 20:10:50 +0200

scythe (0.994+git20141017.20d3cff-3) unstable; urgency=medium

  * Team Upload.
  * Add autopkgtests
  * compat version: 13, standards version: 4.5.0
  * Add "Rules-Requires-Root:no"
  * Add hardening options

 -- Nilesh Patra <npatra974@gmail.com>  Fri, 14 Aug 2020 15:30:00 +0530

scythe (0.994+git20141017.20d3cff-2) unstable; urgency=medium

  * Team upload.
  * Replace python-markdown by markdown
    Closes: #943255
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 16 Dec 2019 10:00:23 +0100

scythe (0.994+git20141017.20d3cff-1) unstable; urgency=medium

  * d/watch: use git mode
  * Drop unneeded get-orig-source target
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.0

 -- Andreas Tille <tille@debian.org>  Fri, 24 Aug 2018 17:50:51 +0200

scythe (0.994-4) unstable; urgency=medium

  * Team upload.
  * Really treat VERSION as string
    Closes: #857900

 -- Andreas Tille <tille@debian.org>  Thu, 16 Mar 2017 14:22:02 +0100

scythe (0.994-3) unstable; urgency=medium

  [ Kevin Murray ]
  * Comment out watchfile, upstream doesn't actually provide tags on github
  * Fix vcs git URL
  * Bump standards version

  [ Mattia Rizzolo ]
  * debian/rules: remove useless include

 -- Kevin Murray <spam@kdmurray.id.au>  Thu, 17 Mar 2016 12:39:24 +0000

scythe (0.994-2) unstable; urgency=low

  * Fix copyright of kseq.h in d/control
  * Patch Makefile to pull in hardening flags correctly
  * Add compiled readme to gitignore

 -- Kevin Murray <spam@kdmurray.id.au>  Mon, 19 Oct 2015 11:27:51 +1100

scythe (0.994-1) unstable; urgency=medium

  * Initial release. (Closes: #776179)

 -- Kevin Murray <spam@kdmurray.id.au>  Sun, 25 Jan 2015 11:09:57 +1100
